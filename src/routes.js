import { createSwitchNavigator, createStackNavigator, createAppContainer } from 'react-navigation'

import Login from './pages/Login'
import Main from './pages/Main'
import ProdListagem from './pages/Produtos/Listagem'
import ProdFormulario from './pages/Produtos/Formulario'
import CliListagem from './pages/Clientes/Listagem'
import CliFormulario from './pages/Clientes/Formulario'

const Routes = createAppContainer(
    createSwitchNavigator({
        Login,
        App: createStackNavigator({
            Main,
            ProdListagem,
            ProdFormulario,
            CliListagem,
            CliFormulario
        }, { initialRouteName: 'Main' })
    }, { initialRouteName: 'Login' })
)

export default Routes