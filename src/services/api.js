import axios from 'axios'

export const ioURL = 'https://minha-marmita-backend.herokuapp.com'

// const localURL = Platform.OS === 'android' ? '10.0.3.2' : 'localhost'

export const api = axios.create({
    // baseURL: 'http://10.0.3.2:4000/api'
    // baseURL: 'http://localhost:4000/api'
    baseURL: ioURL + '/api'
})