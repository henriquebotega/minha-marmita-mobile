import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'stretch',
        paddingHorizontal: 30
    },
    logo: {
        width: 50,
        height: 50,
        alignSelf: 'center',
    },
    item: {
        borderColor: '#F6F6F6',
        borderWidth: 1,
        padding: 10
    },
    itemText: {
        fontWeight: 'bold'
    },
    buttonLogout: {
        marginTop: 50,
        height: 32,
        paddingHorizontal: 20,
        borderRadius: 16,
        backgroundColor: "#FF3366",
        justifyContent: "center",
        alignItems: "center"
    },
    buttonText: {
        color: "#FFF",
        fontSize: 16,
        fontWeight: "bold"
    },

})

export default styles