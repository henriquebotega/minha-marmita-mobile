import React, { Component } from 'react'
import { View, Text, Image, SafeAreaView, TouchableOpacity, AsyncStorage, FlatList } from 'react-native'

import logo from '../../assets/logo.png'
import styles from './styles'

class Main extends Component {
    static navigationOptions = ({
        header: null
    })

    renderItem = (item) => {
        return (
            <TouchableOpacity style={styles.item} onPress={() => this.props.navigation.navigate(item.form, { item })}>
                <View>
                    <Text style={styles.itemText}>{item.titulo}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    handleLogout = async () => {
        await AsyncStorage.removeItem("@MinhaMarmita:logar")
        this.props.navigation.navigate('Login')
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View>
                    <Image style={styles.logo} source={logo} />

                    <FlatList data={[{ id: 1, titulo: 'produtos', form: 'ProdListagem' }, { id: 2, titulo: 'clientes', form: 'CliListagem' }]} keyExtractor={(t) => t.id.toString()} renderItem={({ item }) => this.renderItem(item)} />

                    <TouchableOpacity style={styles.buttonLogout} onPress={() => this.handleLogout()}>
                        <Text style={styles.buttonText}>Sair do sistema</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        )
    }
}

export default Main