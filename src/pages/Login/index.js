import React, { Component } from 'react'
import { Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity, AsyncStorage } from 'react-native'

import styles from './styles'
import Icon from 'react-native-vector-icons/FontAwesome'

class Login extends Component {
    _isMounted = false;

    state = {
        loading: true,
        login: '',
        password: ''
    }

    async componentDidMount() {
        const logar = await AsyncStorage.getItem("@MinhaMarmita:logar")
        this._isMounted = true;

        if (logar) {
            this.props.navigation.navigate('App')
        }

        if (this._isMounted) {
            this.setState({
                loading: false
            })
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleSubmit = async (e) => {
        const { login, password } = this.state

        if (!login || !password) {
            return;
        }

        await AsyncStorage.setItem("@MinhaMarmita:logar", JSON.stringify({ login, password }))
        this.props.navigation.navigate('App')
    }

    handleLogin = (e) => {
        this.setState({ login: e })
    }

    handlePassword = (e) => {
        this.setState({ password: e })
    }

    render() {

        if (this.state.loading) {
            return <Text>Aguarde...</Text>
        }

        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={styles.content}>
                    <View>
                        <Icon name="twitter" size={64} color="#4BB0EE" />
                    </View>

                    <TextInput style={[styles.input, styles.inputLogin]} placeholder="Login" value={this.state.login} onChangeText={(e) => this.handleLogin(e)} />

                    <TextInput secureTextEntry={true} style={styles.input} placeholder="Password" value={this.state.password} onChangeText={(e) => this.handlePassword(e)} />

                    <TouchableOpacity onPress={(e) => this.handleSubmit(e)} style={styles.button}>
                        <Text>Entrar</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        )
    }
}

export default Login