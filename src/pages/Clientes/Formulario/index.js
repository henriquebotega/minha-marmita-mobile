import React, { Component } from 'react'
import { Text, View, SafeAreaView, Image, TouchableOpacity, TextInput, AsyncStorage } from 'react-native'

import { api } from '../../../services/api'
import styles from './styles'

import Icon from 'react-native-vector-icons/MaterialIcons'
import AwesomeAlert from 'react-native-awesome-alerts';

class Formulario extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Detalhes',
        headerLeft: (
            <TouchableOpacity onPress={() => navigation.navigate('CliListagem')}>
                <Icon name="arrow-back" size={24} color="#4BB0EE" style={{ marginLeft: 20 }} />
            </TouchableOpacity>
        )
    })

    _isMounted = false;

    state = {
        showTitle: "Aguarde carregando...",
        showProgress: false,
        showAlert: false,
        registro: {}
    }

    showAlert = () => {
        this.setState({
            showAlert: true,
            showProgress: true
        });
    };

    hideAlert = () => {
        this.setState({
            showAlert: false
        });
    };

    async componentDidMount() {
        this._isMounted = true;
        const item = this.props.navigation.getParam('item');

        if (item) {
            const res = await api.get('/clientes/' + item._id)

            if (this._isMounted) {
                this.setState({
                    registro: res.data
                })
            }
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    handleRemover = async () => {
        this.showAlert();
        const item = this.props.navigation.getParam('item');

        await api.delete('/clientes/' + item._id)

        this.setState({
            showTitle: "Registro removido com sucesso",
            showProgress: false
        });
    }

    handleSalvar = async () => {
        this.showAlert();

        const item = this.props.navigation.getParam('item');
        const registro = this.state.registro

        if (item) {
            await api.put('/clientes/' + item._id, registro)
        } else {
            await api.post('/clientes', registro)
        }

        this.setState({
            showTitle: "Registro salvo com sucesso",
            showProgress: false
        });
    }

    handleNome = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                nome: e
            }
        })
    }

    handleCPF = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                cpf: e
            }
        })
    }

    handleEmail = (e) => {
        this.setState({
            registro: {
                ...this.state.registro,
                email: e
            }
        })
    }

    render() {
        const item = this.props.navigation.getParam('item');
        const { showAlert, showProgress, showTitle } = this.state;
        const { navigation } = this.props;

        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <TextInput style={styles.input} placeholder="Informe o nome" placeholderTextColor="#999" autoCapitalize="none" autoCorrect={false} value={this.state.registro.nome} onChangeText={this.handleNome} underlineColorAndriod="transparent" />

                    <TextInput style={styles.input} placeholder="Informe o Cpf" placeholderTextColor="#999" autoCapitalize="none" autoCorrect={false} value={this.state.registro.cpf} onChangeText={this.handleCPF} underlineColorAndriod="transparent" />

                    <TextInput style={styles.input} placeholder="Informe o e-mail" placeholderTextColor="#999" autoCapitalize="none" autoCorrect={false} value={this.state.registro.email} onChangeText={this.handleEmail} underlineColorAndriod="transparent" />

                    <TouchableOpacity style={styles.button} onPress={this.handleSalvar}>
                        <Text style={styles.buttonText}>Salvar</Text>
                    </TouchableOpacity>

                    {item &&
                        <TouchableOpacity style={styles.buttonRemover} onPress={this.handleRemover}>
                            <Text style={styles.buttonText}>Remover</Text>
                        </TouchableOpacity>
                    }
                </View>

                <AwesomeAlert
                    show={showAlert}
                    showProgress={showProgress}
                    title={showTitle}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    showConfirmButton={!showProgress}
                    confirmText="OK"
                    confirmButtonColor="#48CFAD"
                    onConfirmPressed={() => {
                        if (!showProgress) {
                            navigation.pop()
                        }
                    }}
                />
            </SafeAreaView>
        )
    }
}

export default Formulario