import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF",
    justifyContent: "center",
    alignItems: "center"
  },

  buttonRemover: {
    marginTop: 50,
    height: 32,
    paddingHorizontal: 20,
    borderRadius: 16,
    backgroundColor: "#FF3366",
    justifyContent: "center",
    alignItems: "center"
  },

  button: {
    height: 32,
    paddingHorizontal: 20,
    borderRadius: 16,
    backgroundColor: "#4BB0EE",
    justifyContent: "center",
    alignItems: "center"
  },

  buttonText: {
    color: "#FFF",
    fontSize: 16,
    fontWeight: "bold"
  },

  input: {
    margin: 20,
    fontSize: 16,
    color: "#333",
    width: 350,
    borderWidth: 1,
    borderColor: '#D5D5D5',
  }
});

export default styles