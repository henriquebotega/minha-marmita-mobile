import React, { Component } from 'react'
import { Text, View, SafeAreaView, FlatList, TouchableOpacity, Platform } from 'react-native'

import { api, ioURL } from '../../../services/api'

import socket from 'socket.io-client'
import styles from './styles'

import Icon from 'react-native-vector-icons/MaterialIcons'

class Listagem extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Clientes',
        headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('CliFormulario')}>
                <Icon name="add-circle-outline" size={24} color="#4BB0EE" style={{ marginRight: 20 }} />
            </TouchableOpacity>
        )
    })

    _isMounted = false;

    state = {
        registros: []
    }

    async componentDidMount() {
        this.subscribeToEvents();

        const res = await api.get('/clientes')
        this._isMounted = true;

        if (this._isMounted) {
            this.setState({
                registros: res.data.docs
            })
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    subscribeToEvents = () => {
        const io = socket(ioURL)

        io.on('novoCliente', data => {
            this.setState({
                registros: [data, ...this.state.registros]
            })
        })
    }

    renderItem = (item) => {
        return (
            <TouchableOpacity style={styles.item} onPress={() => this.props.navigation.navigate('CliFormulario', { item })}>
                <View>
                    <Text style={styles.itemText}>{item.nome}</Text>
                    <Text>{item.cpf} - {item.email}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList data={this.state.registros} keyExtractor={(t) => t._id} renderItem={({ item }) => this.renderItem(item)} />
            </SafeAreaView>
        )
    }
}

export default Listagem