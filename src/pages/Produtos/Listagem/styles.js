import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFF"
  },
  item: {
    borderColor: '#D5D5D5',
    borderWidth: 1,
    padding: 10
  },
  itemText: {
    fontWeight: 'bold'
  }
});

export default styles