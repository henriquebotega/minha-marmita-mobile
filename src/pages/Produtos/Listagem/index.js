import React, { Component } from 'react'
import { Text, View, SafeAreaView, FlatList, TouchableOpacity, Platform } from 'react-native'

import { api, ioURL } from '../../../services/api'

import socket from 'socket.io-client'
import styles from './styles'

import Icon from 'react-native-vector-icons/MaterialIcons'

class Listagem extends Component {
    static navigationOptions = ({ navigation }) => ({
        title: 'Produtos',
        headerRight: (
            <TouchableOpacity onPress={() => navigation.navigate('ProdFormulario')}>
                <Icon name="add-circle-outline" size={24} color="#4BB0EE" style={{ marginRight: 20 }} />
            </TouchableOpacity>
        )
    })

    _isMounted = false;

    state = {
        registros: []
    }

    async componentDidMount() {
        this.subscribeToEvents();

        const res = await api.get('/produtos')
        this._isMounted = true;

        if (this._isMounted) {
            this.setState({
                registros: res.data.docs
            })
        }
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    subscribeToEvents = () => {
        const io = socket(ioURL)

        io.on('novoProduto', data => {
            this.setState({
                registros: [data, ...this.state.registros]
            })
        })
    }

    renderItem = (item) => {
        return (
            <TouchableOpacity style={styles.item} onPress={() => this.props.navigation.navigate('ProdFormulario', { item })}>
                <View>
                    <Text style={styles.itemText}>{item.titulo}</Text>
                    <Text>{item.descricao}</Text>
                </View>
            </TouchableOpacity>
        )
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <FlatList data={this.state.registros} keyExtractor={(t) => t._id} renderItem={({ item }) => this.renderItem(item)} />
            </SafeAreaView>
        )
    }
}

export default Listagem