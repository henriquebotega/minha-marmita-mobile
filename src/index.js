import React from 'react'
import Routes from './routes'

import { YellowBox } from 'react-native'
YellowBox.ignoreWarnings(['Unrecognized WebSocket'])
YellowBox.ignoreWarnings(['Async Storage'])
// YellowBox.ignoreWarnings(['Possible Unhandled'])
// YellowBox.ignoreWarnings(['WebView'])

const App = () => <Routes />

export default App